## **Prerequisites**
- Sign in to the Azure portal at https://portal.azure.com.

## Create an AKS cluster

1. On the Azure portal menu or from the Home page, select Create a resource.
- **Project details:**
1. Select Containers > Kubernetes Service.

1. On the Basics page, configure the following options:

    - Select an Azure Subscription.
    - Select or create an Azure Resource group, such as myResourceGroup.
- **Cluster details**
    - Ensure the the Preset configuration is Standard ($$). For more details on preset configurations, see Cluster configuration presets in the Azure portal.

    - Enter a Kubernetes cluster name, such as myAKSCluster.

    - Select a Region and Kubernetes version for the AKS cluster.

- **Primary node pool:**
    - Leave the default values selected.

![Image](https://docs.microsoft.com/en-us/azure/aks/media/kubernetes-walkthrough-portal/create-cluster-basics.png){width=50%}

4. Select Next: Node pools when complete

5. Keep the default Node pools options. At the bottom of the screen, click Next: Authentication.

6.On the Authentication page, configure the following options:

- Create a new cluster identity by 
either:

    - Leaving the Authentication field with System-assinged managed identity,
    
    or

    - Choosing Service Principal to use a service principal
      
      - Select (new) default service principal to create a default service principal,

         or

      - Select Configure service principal to use an existing one. You will need to provide the existing principal's SPN client ID and secret.

- Enable the Kubernetes role-based access control (Kubernetes RBAC) option to provide more fine-grained control over access to the Kubernetes resources deployed in your AKS cluster.

- By default, Basic networking is used, and Azure Monitor for containers is enabled.


7. Click Review + create and then Create when validation completes

8.It takes a few minutes to create the AKS cluster. When your deployment is complete, navigate to your resource by either:


- Clicking Go to resource, 
- Browsing to the AKS cluster resource group and selecting the AKS resource.

   - Per example cluster dashboard below: browsing for myResourceGroup and selecting myAKSCluster resource.

![image](https://docs.microsoft.com/en-us/azure/aks/media/kubernetes-walkthrough-portal/aks-portal-dashboard.png)


## **Connect to the cluster**

<p>To manage a Kubernetes cluster, use the Kubernetes command-line client, kubectl. kubectl is already installed if you use Azure Cloud Shell.</P>

  1. Open Cloud Shell using the >_ button on the top of the Azure portal.

  1. Connect to Azure via the az login command

1. Configure kubectl to connect to your Kubernetes cluster using the az aks get-credentials command. The following command downloads credentials and configures the Kubernetes CLI to use them.

```
az aks get-credentials --resource-group myResourceGroup --name myAKSCluster
```
1. Verify the connection to your cluster using kubectl get to return a list of the cluster nodes.

```
kubectl get nodes
```
**<p>Output shows the single node created</p>
  
    
